﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace PirateJam
{
    public static class Program
    {
        [STAThread]
        private static void Main()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            var config = builder.Build();
            
            using var game = new Game1(config);
            game.Run();
        }
    }
}